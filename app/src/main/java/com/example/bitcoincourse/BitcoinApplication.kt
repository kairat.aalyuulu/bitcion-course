package com.example.bitcoincourse

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.example.bitcoincourse.di.netWorkModule
import com.example.bitcoincourse.di.repositoryModule
import com.example.bitcoincourse.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class BitcoinApplication : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        context = this

        startKoin {
            androidLogger(Level.NONE)
            androidContext(this@BitcoinApplication)
            modules(
                listOf(
                    repositoryModule,
                    netWorkModule,
                    viewModelModule
                )
            )
        }
    }
}