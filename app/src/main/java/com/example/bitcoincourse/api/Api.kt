package com.example.bitcoincourse.api

import com.example.bitcoincourse.model.BitcoinModel
import retrofit2.Response
import retrofit2.http.GET

interface Api {

    @GET("prices/BTC-USD/spot")
    suspend fun getValue(): Response<BitcoinModel>
}