package com.example.bitcoincourse.di

import com.example.bitcoincourse.repo.BitcoinRepo
import org.koin.dsl.module

val repositoryModule = module {
    single { BitcoinRepo(api = get() ) }
}