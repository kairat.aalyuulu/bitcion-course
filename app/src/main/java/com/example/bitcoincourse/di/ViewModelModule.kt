package com.example.bitcoincourse.di

import com.example.bitcoincourse.main.MainViewModel
import org.koin.dsl.module

val viewModelModule = module {
    single { MainViewModel(get()) }
}