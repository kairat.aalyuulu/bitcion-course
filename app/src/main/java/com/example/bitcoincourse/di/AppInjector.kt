package com.example.bitcoincourse.di

import com.example.bitcoincourse.api.Api
import org.koin.dsl.module
import retrofit2.Retrofit

private val retrofit: Retrofit = createNetworkClient()

private val REST_API: Api = retrofit.create(Api::class.java)

val netWorkModule = module {
    single { REST_API }
}