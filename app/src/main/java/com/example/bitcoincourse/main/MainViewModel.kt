package com.example.bitcoincourse.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bitcoincourse.model.BitcoinModel
import com.example.bitcoincourse.model.DataModel
import com.example.bitcoincourse.repo.BitcoinRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(private val repo: BitcoinRepo) : ViewModel() {

    var bitcoinLiveData = MutableLiveData<DataModel>()
    var errorMessageLiveData = MutableLiveData<String>()
    var loadingVisibility = MutableLiveData<Boolean>()
    private val coroutine: CoroutineScope= CoroutineScope(Dispatchers.Main)

    fun getValue(){
        coroutine.launch {
            loadingVisibility.value = true
            val response = repo.getValue()
            if (response != null) {

                if (response.isSuccessful) {
                    bitcoinLiveData.value = response.body()?.data
                }
            }
            loadingVisibility.value = false
        }
    }

}