package com.example.bitcoincourse

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.example.bitcoincourse.main.MainViewModel
import com.example.bitcoincourse.model.DataModel
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var textView: TextView
    private lateinit var textViewSmall: TextView
    private lateinit var button: Button

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }



    private fun init() {
        configureMainViewModel()


        textViewSmall = findViewById(R.id.tv_btc_value_small)
        textView = findViewById(R.id.tv_btc_value_big)
        button = findViewById(R.id.refresh_button)

        button.setOnClickListener {
            viewModel.getValue()
        }


    }
    private fun configureMainViewModel() {

        viewModel.errorMessageLiveData.observe(
            this,
            {
                Toast(this).show()
            }
        )

        viewModel.bitcoinLiveData.observe(
            this,
            {
                setData(data = it)
            }
        )
        viewModel.getValue()
    }

    @SuppressLint("SetTextI18n")
    private fun setData(data: DataModel) {
        textView.text = data.amount?.substring(0,2)+"."+data.amount?.substring(2,5)
        textViewSmall.text = data.amount?.substring(5)
    }
}