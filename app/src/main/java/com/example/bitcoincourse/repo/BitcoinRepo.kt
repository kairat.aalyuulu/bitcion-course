package com.example.bitcoincourse.repo

import com.example.bitcoincourse.api.Api
import com.example.bitcoincourse.model.BitcoinModel
import retrofit2.Response

class BitcoinRepo(private val api: Api) {

    suspend fun getValue(): Response<BitcoinModel>?{
        return try {
            return api.getValue()
        } catch (e: Exception){
            null
        }
    }
}